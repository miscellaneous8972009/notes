This is a quick note of some heavily used Markdown syntaxes.

Guide: [markdownguide.org](https://www.markdownguide.org/)
Online visual editor: [Dillinger](https://dillinger.io/)

# Header

Put a \# in front of the header. The number of \#'s corresponds to `<h1>` to `<h6>` in HTML.

# Bold

Put \** or \__ around the texts. e.g. **bold texts**

# Italic

Put \* or \_ around the texts. e.g. *italic texts*
